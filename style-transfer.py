import boto3
import json
import cv2 as cv
import time
from botocore.exceptions import ClientError
import logging
import os
import uuid
from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError

BUCKET = "hw7-bucket"

session = boto3.session.Session(
    aws_access_key_id="UiVyPxFsbkZdE9Rn5rp5",
    aws_secret_access_key="RK1zzbqnB7Rux1bdCgOBQjTbjlCBCySULTP_VEdl",
)

s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

def create_presigned_url(bucket_name, object_name, expiration=3600):
    # Generate a presigned URL for the S3 object
    try:
        response = s3.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': object_name},
                                                    ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL
    return response
  
def predict(net, img, h, w):
    blob = cv.dnn.blobFromImage(img, 1.0, (w, h),
                                (103.939, 116.779, 123.680), swapRB=False, crop=False)

    print('[INFO] Setting the input to the model')
    net.setInput(blob)

    print('[INFO] Starting Inference!')
    start = time.time()
    out = net.forward()
    end = time.time()
    print('[INFO] Inference Completed successfully!')

    # Reshape the output tensor and add back in the mean subtraction, and
    # then swap the channel ordering
    out = out.reshape((3, out.shape[2], out.shape[3]))
    out[0] += 103.939
    out[1] += 116.779
    out[2] += 123.680
    out /= 255.0
    out = out.transpose(1, 2, 0)

    # Printing the inference time
    print('[INFO] The model ran in {:.4f} seconds'.format(end - start))

    return out


# Source for this function:
# https://github.com/jrosebr1/imutils/blob/4635e73e75965c6fef09347bead510f81142cf2e/imutils/convenience.py#L65
def resize_img(img, width=None, height=None, inter=cv.INTER_AREA):
    dim = None
    h, w = img.shape[:2]

    if width is None and height is None:
        return img
    elif width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    elif height is None:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv.resize(img, dim, interpolation=inter)
    return resized


def process_image(image, model, output):
    net = cv.dnn.readNetFromTorch(model)
    img = cv.imread(image)
    img = resize_img(img, width=600)
    h, w = img.shape[:2]
    out = predict(net, img, h, w)
    out = cv.convertScaleAbs(out, alpha=255.0)
    cv.imwrite(output, out)

models_by_styles = {
  "feathers": "feathers.t7",
  "mosaic": "mosaic.t7",
  "scream": "the_scream.t7"
}

def prepare_models():
  folder = "models/"
  for mname in models_by_styles.values():
    objectpath = folder + mname
    print("Downloading model %s ..." % objectpath)
    dest_filepath = "/tmp/" + mname
    s3.download_file(Bucket=BUCKET, Key=objectpath, Filename=dest_filepath)

def gen_filename():
  return str(uuid.uuid4())


ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
DATABASE = "/ru-central1/b1gao5nvuklp7p1a4p2u/etn01hb9u7r0102jkhi7"

def get_db_session():
  driver_config = ydb.DriverConfig(ENDPOINT, DATABASE, credentials=ydb.construct_credentials_from_environ(),
      root_certificates=ydb.load_ydb_root_certificate(),
  )
  driver = ydb.Driver(driver_config)
  try:
      driver.wait(timeout=5)
  except TimeoutError:
      print("Connect failed to YDB")
      print("Last reported errors by discovery:")
      print(driver.discovery_debug_details())
      exit(1)

  print("Connect to YDB is OK!")
  
  session = driver.table_client.session().create()
  return session


def finish_db_item(task_uid, download_link):
  session = get_db_session()
  session.transaction().execute(
      "UPSERT INTO tasks (task_uid, status, download_link) VALUES ('%s', 'DONE', '%s');" % (task_uid, download_link), 
      commit_tx=True)


def handler(event, context):
  res = None
  prepare_models()
  inner_msg = event['messages'][0]['details']['message']['body']
  try:
    request = json.loads(inner_msg)
    object_name = request['object_name']
    task_uid = os.path.basename(object_name)
    
    style = request['style']
    print(f"Got object name '{object_name}' and style '{style}'")
    
    tmp_image_filename = gen_filename()
    
    dest_filepath = "/tmp/" + tmp_image_filename + ".jpg"
    print(dest_filepath)
    s3.download_file(Bucket=BUCKET, Key=object_name, Filename=dest_filepath)
    
    style = "feathers" # todo: use style from request
    modelname = models_by_styles[style]
    outpath = "/tmp/" + tmp_image_filename + "_processed.jpg"
    modelpath = "/tmp/" + modelname
    process_image(dest_filepath, modelpath, outpath)
    
    res_obj_name = "processed_images/" + os.path.basename(object_name)
    s3.upload_file(Bucket=BUCKET, Key=res_obj_name, Filename=outpath)

    response = create_presigned_url('hw7-bucket', res_obj_name)
    
    finish_db_item(task_uid, response)
    
    body = json.dumps({'presigned_post': response, 'filename': res_obj_name})
    
    res = {
      'statusCode': 200,
      'body': body,
    }
    
  except Exception as e:
    print(e)

  return res