import boto3
import json
from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError
import os

BUCKET = "hw7-bucket"

session = boto3.session.Session(
    aws_access_key_id="UiVyPxFsbkZdE9Rn5rp5",
    aws_secret_access_key="RK1zzbqnB7Rux1bdCgOBQjTbjlCBCySULTP_VEdl",
)

s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

MAX_CONTENT_LEN = 16 * 1024 * 1024

def is_object_correct(obj_filename):
  response = s3.get_object(Bucket=BUCKET, Key=obj_filename)
  
  print("Object content len is: {}".format(response['ContentLength']))
  
  if int(response['ContentLength']) > MAX_CONTENT_LEN:
    return False

  return True


ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
DATABASE = "/ru-central1/b1gao5nvuklp7p1a4p2u/etn01hb9u7r0102jkhi7"

def get_db_session():
  driver_config = ydb.DriverConfig(ENDPOINT, DATABASE, credentials=ydb.construct_credentials_from_environ(),
      root_certificates=ydb.load_ydb_root_certificate(),
  )
  driver = ydb.Driver(driver_config)
  try:
      driver.wait(timeout=5)
  except TimeoutError:
      print("Connect failed to YDB")
      print("Last reported errors by discovery:")
      print(driver.discovery_debug_details())
      exit(1)

  print("Connect to YDB is OK!")
  
  session = driver.table_client.session().create()
  return session

def update_on_processing(task_uid):
  session = get_db_session()
  session.transaction().execute(
      "UPSERT INTO tasks (task_uid, status, download_link) VALUES ('%s', '%s', '');" % (task_uid, "PROCESSING"), 
      commit_tx=True)

def handler(event, context):

  obj_name = event['messages'][0]["details"]['object_id']
  task_uid = os.path.basename(obj_name)

  res = is_object_correct(obj_name)
  if res == False:
    s3.remove_object(Bucket=BUCKET, Key=obj_name)
    return {
      'statusCode': 400,
      'body': "Object is removed!",
    }

  client = boto3.client(
      service_name='sqs',
      endpoint_url='https://message-queue.api.cloud.yandex.net',
      region_name='ru-central1',
      aws_access_key_id="UiVyPxFsbkZdE9Rn5rp5",
      aws_secret_access_key="RK1zzbqnB7Rux1bdCgOBQjTbjlCBCySULTP_VEdl",
  )
  
  client.send_message(
    QueueUrl="https://message-queue.api.cloud.yandex.net/b1gao5nvuklp7p1a4p2u/dj6000000001nq7404ig/ready-to-apply",
    MessageBody=json.dumps({
             "object_name": obj_name,
             "style": "feathers"
           })
  )
  
  update_on_processing(task_uid)
  
  print("Successfully sent! ...")
  
  return {
    'statusCode': 200,
    'body': 'OK!'
  }
    
  