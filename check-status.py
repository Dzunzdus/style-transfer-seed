from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError

ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
DATABASE = "/ru-central1/b1gao5nvuklp7p1a4p2u/etn01hb9u7r0102jkhi7"

def get_db_session():
  driver_config = ydb.DriverConfig(ENDPOINT, DATABASE, credentials=ydb.construct_credentials_from_environ(),
      root_certificates=ydb.load_ydb_root_certificate(),
  )
  driver = ydb.Driver(driver_config)
  try:
      driver.wait(timeout=5)
  except TimeoutError:
      print("Connect failed to YDB")
      print("Last reported errors by discovery:")
      print(driver.discovery_debug_details())
      exit(1)

  print("Connect to YDB is OK!")
  
  session = driver.table_client.session().create()
  return session

def get_info(task_uid):
  session = get_db_session()
  result = session.transaction().execute(
      "SELECT (task_uid, status, download_link) FROM tasks WHERE task_uid = '{}' LIMIT 1;".format(task_uid))
  return result[0].rows

def handler(event, context):
  
  if "task_uid" not in event['queryStringParameters']:
    return {
        'statusCode': 401,
        'body': "Can't find task_uid param in query",
    }
  
  task_uid = event['queryStringParameters']["task_uid"]
  
  info = get_info(task_uid)
  
  if info is None:
    return {
      'statusCode': 401,
      'body': "Can't get info for this task_uid"
    }
  
  return {
    'statusCode': 200,
    'body': info
  }
  