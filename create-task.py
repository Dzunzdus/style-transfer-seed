import boto3
import uuid
import logging
import json
from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError

session = boto3.session.Session(
    aws_access_key_id="UiVyPxFsbkZdE9Rn5rp5",
    aws_secret_access_key="RK1zzbqnB7Rux1bdCgOBQjTbjlCBCySULTP_VEdl",
)

s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

def create_presigned_post(bucket_name, object_name,
                          fields=None, conditions=None, expiration=3600):
    try:
        response = s3.generate_presigned_post(bucket_name,
                                                     object_name,
                                                     Fields=fields,
                                                     Conditions=conditions,
                                                     ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL and required fields
    return response

ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
DATABASE = "/ru-central1/b1gao5nvuklp7p1a4p2u/etn01hb9u7r0102jkhi7"

def put_item_in_ydb(item):
    driver_config = ydb.DriverConfig(ENDPOINT, DATABASE, credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    with ydb.Driver(driver_config) as driver:
        try:
            driver.wait(timeout=5)
        except TimeoutError:
            print("Connect failed to YDB")
            print("Last reported errors by discovery:")
            print(driver.discovery_debug_details())
            exit(1)
    
        print("Connect to YDB is OK!")
        
        session = driver.table_client.session().create()
        
        session.transaction().execute(
            "UPSERT INTO tasks (task_uid, status, download_link) VALUES ('%s', '%s', '');" % (item['task_uid'], item['status']), 
            commit_tx=True)
    
def handler(event, context): 
    task_uid = str(uuid.uuid4())
    obj_name = "raw_images/" + task_uid
    
    put_item_in_ydb({"task_uid": task_uid, "status": "NEW", "download_link": ""})

    response = create_presigned_post('hw7-bucket', obj_name)
    body = json.dumps({'presigned_post': response, 'filename': obj_name})
    return {
        'statusCode': 200,
        'body': body,
    }

 